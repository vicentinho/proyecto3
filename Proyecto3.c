#include <stdio.h>
#include <stdlib.h>
#define largo 54   			//declaracion variables
int numero;
char temp[50];
int count;
int x,y,a,j,i;
int contadores[15];
struct ponderaciones_18{     // declaracion de la estructura
int	nem;
int rank;
int leng;
int mat;
int hist;
int cs;
};
struct puntaje_minimo{
int pond;
int psu;
	
};
struct puntaje_17{
float max;
float min;
};
struct cupo{
int psu;
int bea;
};
struct carrera{
	char facultad[70];
	char nombre[100];
	int codigo;
	struct ponderaciones_18	porcentaje;	
	struct puntaje_minimo 	minimo_pedido;
	struct puntaje_17 		puntajes_anteriores;
	struct cupo 			cupos;
}carreras[largo];

char menu(){
int z=1;
while(z==1){		//menu

	char opcion;
	printf("------Bienvenido------\n");
	printf("Ingrese su opción de acuerdo a segun consulta.\n");
	printf("1) Consultar ponderacion de carrera.\n");
	printf("2) Simular postulacion carrera.\n");
	printf("3) Mostrar ponderacion facultad.\n");
	printf("s) Salir del programa.\n");
	scanf("%s",&opcion);
	if(opcion!='s' && opcion !='1' && opcion !='2' && opcion!= '3'){ 		// este menu solo recibe 4 opciones 's' ,'1' ,'2', '3'
		printf("Opcion errada, ingrese nuevamente su opcion:\n");
	}else{
		return opcion;
		z=0;
	}
}
}


int main(){
 	FILE *archivo;						
 	
 	char caracteres[1000];			//declaraciones para poder abrir/trabajar con el archivo.txt
	char *trozo;
	int i;	
 	archivo = fopen("Carreras.txt","r");		// abrimos el archivo.txt
 	
 	if (archivo == NULL){							// verificamos si es posible abrir el archivo
 		printf("el archivo no pudo ser cargado");
		x=0;	// variable de control para no correr el menu si el archivo no pudo ser cargado
 	}else{
 		x=1;
 		i=0;
 	    while (feof(archivo) == 0){		  	    	// recorreremos el archivo linea por linea hasta que se quede sin caracteres
 			fgets(caracteres,1000,archivo);			
 			
			count=strlen(caracteres);		
			x=0;
 			for(j=0;j<count;j++){
 				
 				if(caracteres[j]=='-'){				// creamos un arreglo donde almacenar los espacios donde se encuentra cada'-' ya que 
 					contadores[x++]=j; 				// el documento de texto "carreras.txt" y de este modo ir recorriendo y usando los caracteres
				}									// entre "-" para almacenar estos segementos de texto dentro de la estructura 
 			}
 		y=0;
		for(j=0;j<contadores[0];j++){
			carreras[i].facultad[y++]=caracteres[j];	//almacenamos la facultadad caracter por caracter
 			}
 			
 		y=0;	
 		for(j=contadores[0]+1;j<contadores[1];j++){   //almacenamos el nombre de la carrera caracter por caracter
			carreras[i].nombre[y++]=caracteres[j];
 			}
 
 		y=0;
 		memset(temp,'\0',50);
 		for(j=contadores[1]+1;j<contadores[2];j++){  	//almacenamos el codigo de la carrera caracter por caracter
			temp[y++]=caracteres[j];
 			}							
		carreras[i].codigo=atoi(temp); // pero en este caso y en los demas siguientes trasformamos los segmentos de string en entereos y estos son almacenados dentro de su variable respectiva
			
 		y=0;
		memset(temp,'\0',50);
		for(j=contadores[2]+1;j<contadores[3];j++){    // almacenamiento multiples-variables
 			temp[y++]=caracteres[j];
			}
 		carreras[i].porcentaje.nem=atoi(temp);
			
		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[3]+1;j<contadores[4];j++){
			temp[y++]=caracteres[j];
 			}
		carreras[i].porcentaje.rank=atoi(temp);
			
		y=0;
		memset(temp,'\0',50);
		for(j=contadores[4]+1;j<contadores[5];j++){ 
			temp[y++]=caracteres[j];
			}
		carreras[i].porcentaje.leng=atoi(temp);
 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[5]+1;j<contadores[6];j++){ 	
			temp[y++]=caracteres[j];
			}
 		carreras[i].porcentaje.mat=atoi(temp);
 			
 		y=0;
		memset(temp,'\0',50);
 			for(j=contadores[6]+1;j<contadores[7];j++){ 
			temp[y++]=caracteres[j];
 			}
 		carreras[i].porcentaje.hist=atoi(temp);
 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[7]+1;j<contadores[8];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].porcentaje.cs=atoi(temp);
 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[8]+1;j<contadores[9];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].minimo_pedido.pond=atoi(temp);
 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[9]+1;j<contadores[10];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].minimo_pedido.psu=atoi(temp);
 			 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[10]+1;j<contadores[11];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].puntajes_anteriores.max=atof(temp);
 			 			
 		y=0;
		memset(temp,'\0',50);
 			for(j=contadores[11]+1;j<contadores[12];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].puntajes_anteriores.min=atof(temp);
 			 			
 		y=0;
		memset(temp,'\0',50);
 		for(j=contadores[12]+1;j<contadores[13];j++){
			temp[y++]=caracteres[j];
			}
		carreras[i].cupos.psu=atoi(temp);
 						
		y=0;
		memset(temp,'\0',50);
		for(j=contadores[13]+1;j<contadores[14];j++){			//fin de almacenamiento de variables
			temp[y++]=caracteres[j];
			}
		carreras[i].cupos.bea=atoi(temp);
 		i++; 		
		}
	x=1;
    }
    fclose(archivo); // cerramos el archivo como buenas costumbres
////////////////////////////////////////////////////////////

	char op;
	while(x==1){
		op=menu();
		if(op=='1'){ // opcion 1 del menu para desplegar la informacion de una X carrera atravez de su codigo
			int codigo,control;
			printf("Ingrese el codigo de la carrera:\n");
			scanf("%d",&codigo);
			control=1;
			for (i=0;i<largo;i++){	
				if(codigo==carreras[i].codigo){ // si es contrado el codigo dentro del arreglo mostraremos los demas datos respectivos a la misma carrera
					control=0;
					printf("Facultad:%s\n",carreras[i].facultad);
					printf("Su carrera es:%s\n",carreras[i].nombre);
					printf("El Nem de carrera:%d % \n",carreras[i].porcentaje.nem);
					printf("El Ranking de carrera:%d % \n",carreras[i].porcentaje.rank);
					printf("El puntaje de lenguaje es:%d % \n",carreras[i].porcentaje.leng);
					printf("El puntaje de matematicas es:%d % \n",carreras[i].porcentaje.mat);
					printf("El puntaje de historia es:%d % \n",carreras[i].porcentaje.hist);
					printf("El puntaje de ciencias es:%d % \n",carreras[i].porcentaje.cs);
					printf("Ultimo seleccionado 2016: %.2f \n", carreras[i].puntajes_anteriores.min);
					printf("Vacantes:%d\n", carreras[i].cupos.psu);								
					break;
					}
				}
			
			if(control==1){
				printf("la carrera ingresada no ha sido encontrada\n");
			}
					
		}	
		if(op=='2'){	// segunda opcion con esta opcion entramos a la simulacion de datos
			int nem,rank,codigo,leng,mate,histo,cs,i,control;
			float resultado,diferencia;
			// solicitamos los datos al usuario
			printf("Si no ha rendido una prueba, ingrese 0\n");
			printf("Ingrese el codigo de la carrera:\n");
			scanf("%d",&codigo);			
			printf("Ingrese su Nem:\n");
			scanf("%d",&nem);
			printf("Ingrese su Ranking:\n");
			scanf("%d",&rank);
			printf("Ingrese su puntaje de Lenguaje:\n");
			scanf("%d",&leng);
			printf("Ingrese su puntaje de Matematicas:\n");
			scanf("%d",&mate);
			printf("Ingrese su puntaje de Historia:\n");
			scanf("%d",&histo);
			printf("Ingrese su puntaje de Ciencias:\n");
			scanf("%d",&cs);
			control=1;
			for (i=0;i<largo;i++){	
				if(codigo==carreras[i].codigo){ // si es encontrada la carrera, mostramos los datos y hacemos los calculos 
					control=0;
					printf("%s\n",carreras[i].facultad);
					printf("Su carrera es:%s\n",carreras[i].nombre);
					printf("El Nem de carrera:%d % \n",carreras[i].porcentaje.nem);
					printf("El Ranking de carrera:%d % \n",carreras[i].porcentaje.rank);
					printf("El puntaje de lenguaje es:%d % \n",carreras[i].porcentaje.leng);
					printf("El puntaje de matematicas es:%d % \n",carreras[i].porcentaje.mat);
					printf("El puntaje de historia es:%d % \n",carreras[i].porcentaje.hist);
					printf("El puntaje de ciencias es:%d % \n",carreras[i].porcentaje.cs);
					if(((float)histo*((float)carreras[i].porcentaje.hist/(float)100)) > ((float)cs*((float)carreras[i].porcentaje.cs/(float)100))){
					resultado=(float)(nem*((float)carreras[i].porcentaje.nem/(float)100)) + ((float)rank*((float)carreras[i].porcentaje.rank/(float)100))+ ((float)leng*((float)carreras[i].porcentaje.leng/(float)100))+ ((float)mate*((float)carreras[i].porcentaje.mat/(float)100 ))+ ((float)histo*((float)carreras[i].porcentaje.hist/(float)100));	
					}else{
					resultado=(float)(nem*((float)carreras[i].porcentaje.nem/(float)100)) + ((float)rank*((float)carreras[i].porcentaje.rank/(float)100))+ ((float)leng*((float)carreras[i].porcentaje.leng/(float)100))+ ((float)mate*((float)carreras[i].porcentaje.mat/(float)100 ))+ ((float)cs*((float)carreras[i].porcentaje.cs/(float)100));
					}
					diferencia=resultado - carreras[i].puntajes_anteriores.min;	
					printf("Su puntaje simulado:%.2f\n",resultado);							//mostramos los datos calculados de la simulacion
					printf("Ultimo seleccionado 2016: %.2f \n", carreras[i].puntajes_anteriores.min);
					printf("Diferencia:%.2f\n",diferencia);
					printf("Vacantes:%d\n", carreras[i].cupos.psu);								
				
					if(carreras[i].minimo_pedido.pond>resultado || carreras[i].minimo_pedido.psu>((mate+leng)/2)){ // si los resultados de la simulacion no cumplen con lo minimo para la postulacion pues le informamos al usuario que no puede postular
						printf("Su puntaje ponderado, no alcanza para la postulación minima\n");
					break;
					}
				}
			}
			if(control==1){
				printf("la carrera ingresada no ha sido encontrada\n"); // si la carrera no es encontrada o el usuario ingreso un codigo no existente
			}	
			
		}
		if(op=='3'){ // opcion 3 mostramos una tabla con todos los datos de una facultad (agrandar la terminal y reducir la letra a 14o 12 depende de la pantalla del equipo)
		a=0;
		while(a==0){
		
		char opcion;
		printf("Seleccione la facultadad a Consultar\n"); // desplegamos el menu de facultades 
		printf("1) FACULTAD DE ARQUITECTURA\n");
		printf("2) FACULTAD DE CIENCIAS\n");
		printf("3) FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES\n");
		printf("4) FACULTAD DE CIENCIAS ECONOMICAS Y ADMINISTRATIVAS\n");
		printf("5) FACULTAD DE DERECHO Y CIENCIAS SOCIALES\n");
		printf("6) FACULTAD DE FARMACIA\n");
		printf("7) FACULTAD DE HUMANIDADES\n");
		printf("8) FACULTAD DE INGENIERIA\n");
		printf("9) FACULTAD DE MEDICINA\n");
		printf("0) FACULTAD DE ODONTOLOGIA\n");	
		printf("ingrese cualquier tecla para volver al menu");	
		scanf("%s",&opcion);
		if(opcion=='1'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE ARQUITECTURA", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);			// para ordenar la tabla ingresamod tabulaciones dependiendo del largo de el nombre ya que este es el unico que varia y corre los datos de las tablas
					if(strlen(carreras[i].nombre)<8)printf("\t");  // ya como las tabulaciones son de 8 espacios vamos en 8 en 8
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
						// en las demas opciones es lo mismo explicado solamente cambia el filtro
						
		}else if(opcion=='2'){
			printf("carrera\t\t\t\t\t\t\t\t\t\t\tcodigo\tnem\t\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE CIENCIAS", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
					if(strlen(carreras[i].nombre)<40)printf("\t");
					if(strlen(carreras[i].nombre)<48)printf("\t");
					if(strlen(carreras[i].nombre)<56)printf("\t");
					if(strlen(carreras[i].nombre)<64)printf("\t");
					if(strlen(carreras[i].nombre)<72)printf("\t");
					if(strlen(carreras[i].nombre)<80)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='3'){
			printf("carrera\t\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
					if(strlen(carreras[i].nombre)<40)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='4'){
			printf("carrera\t\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE CIENCIAS ECONOMICAS Y ADMINISTRATIVAS", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
					if(strlen(carreras[i].nombre)<40)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='5'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE DERECHO Y CIENCIAS SOCIALES", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='6'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE FARMACIA", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='7'){
			printf("carrera\t\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE HUMANIDADES", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
					if(strlen(carreras[i].nombre)<40)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='8'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE INGENIERIA", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='9'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE MEDICINA", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else if(opcion=='0'){
			printf("carrera\t\t\t\t\tcodigo\tnem\trank\tlen\tmate\thist\tcs\tpond\tpsu\tmax2017\tmin2017\tcupos\tbea\n");
			for(i=0;i<largo;i++){
				if(strcmp("FACULTAD DE ODONTOLOGIA", carreras[i].facultad)==0){
					printf("%s",carreras[i].nombre);
					if(strlen(carreras[i].nombre)<8)printf("\t");
					if(strlen(carreras[i].nombre)<16)printf("\t");
					if(strlen(carreras[i].nombre)<24)printf("\t");
					if(strlen(carreras[i].nombre)<32)printf("\t");
				printf("\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%d\t\t%d\n", carreras[i].codigo , carreras[i].porcentaje.nem , carreras[i].porcentaje.rank , carreras[i].porcentaje.leng , carreras[i].porcentaje.mat , carreras[i].porcentaje.hist , carreras[i].porcentaje.cs , carreras[i].minimo_pedido.pond , carreras[i].minimo_pedido.psu , carreras[i].puntajes_anteriores.max , carreras[i].puntajes_anteriores.min , carreras[i].cupos.psu , carreras[i].cupos.bea);
				}
			}
			
		}else{	// volvemos al menu
		printf("La opcion ingresada no es valida, Sera devuelto al menu\n");
			a=1;		
			}					
		}
		}
		if(op=='s'){ // salimos del programa 
		printf("Fuiste weno :P\n"); // fuiste bueno* 
		x=0; // variable control para cerrar los ciclos
		}
	}
}


